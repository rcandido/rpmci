# Changelog

Please take note of the following changes, they may require changes to your `.gitlab-ci.yml` files:

### 2020-03-03
 - de3c839: The `builder-cc7` image no longer includes the repos for Software Collections. If you wish to use them, you will need
   to add a `yum install -y centos-release-scl` to your gitlab jobs.

### 2020-02-17
 - f388493: `test_rpmlint` was renamed to `.test_rpmlint`.

### 2020-01-28
 - c925722: Changed default value of `BUILD_7` to `False`. Current users may have to adjust their configuration if they relied on the default.
