---
variables:
  _KOJI_PROFILE: 'koji'
  IMAGE_KOJI: 'gitlab-registry.cern.ch/linuxsupport/rpmci/kojicli'
  IMAGE_8: 'gitlab-registry.cern.ch/linuxsupport/rpmci/builder-c8'
  IMAGE_7: 'gitlab-registry.cern.ch/linuxsupport/rpmci/builder-cc7'
  IMAGE_6: 'gitlab-registry.cern.ch/linuxsupport/rpmci/builder-slc6'
  BUILD_8: 'False'
  BUILD_7: 'False'
  BUILD_6: 'False'
  DIST_8: '.el8'
  DIST_7: '.el7'
  DIST_6: '.slc6'
  KOJI_TAG_8: ''
  KOJI_TAG_7: ''
  KOJI_TAG_6: ''

include:
  - 'https://gitlab.cern.ch/linuxsupport/rpmci/raw/master/dist_8.yml'
  - 'https://gitlab.cern.ch/linuxsupport/rpmci/raw/master/dist_7.yml'
  - 'https://gitlab.cern.ch/linuxsupport/rpmci/raw/master/dist_6.yml'

stages:
  - prebuild
  - srpm
  - rpm
  - lint
  - prekoji
  - koji_scratch
  - pretest
  - test
  - posttest
  - koji_build
  - postkoji
  - docker_prepare
  - docker_build
  - deploy_qa
  - deploy_stable

.rpm_deps:
  image: $IMAGE
  interruptible: true
  before_script:
    - yum-builddep -y *.spec
  dependencies: []

.build_srpm:
  extends: .rpm_deps
  stage: srpm
  script:
    - export MAKEFILE=`[ -e Makefile.koji ] && echo "Makefile.koji" || echo "Makefile"`
    - make -f $MAKEFILE srpm
    - |
      if [[ ! -d build/SRPMS/ ]]; then
        echo "build/SRPMS/ not found, please check your Makefile and your .spec"
        exit 1
      fi
  artifacts:
    expose_as: 'Source RPM'
    paths:
      - build/SRPMS/
    expire_in: 1 month

.build_rpm:
  extends: .rpm_deps
  stage: rpm
  script:
    - export MAKEFILE=`[ -e Makefile.koji ] && echo "Makefile.koji" || echo "Makefile"`
    - make -f $MAKEFILE rpm
    - |
      if [[ ! -d build/RPMS/ ]]; then
        echo "build/RPMS/ not found, please check your Makefile and your .spec"
        exit 1
      fi
  artifacts:
    paths:
      - build/RPMS/
    expire_in: 1 day

.test_rpmlint:
  image: $IMAGE
  stage: lint
  interruptible: true
  script:
    - |
      if [[ -e .rpmlint ]]; then
        echo "Using custom rpmlint configuration from .rpmlint"
        CONFIG="-f .rpmlint"
      else
        echo "Using default rpmlint configuration. Create a .rpmlint file to override settings"
      fi
    - rpmlint -i $CONFIG *.spec
    - rpmlint -i $CONFIG build/

.test_install:
  image: $IMAGE
  stage: test
  interruptible: true
  before_script:
    - export _KOJITAG_OS="KOJI_TAG_${_KOJI_OS}"
    - export _KOJITAG=${!_KOJITAG_OS:-"${KOJI_TAG}${_KOJI_OS}"}
    - yum install -y --nogpgcheck `ls koji/*${DIST}.noarch.rpm koji/*${DIST}.x86_64.rpm`
  script:
    - echo "Test me!"

.koji_deps:
  image: $IMAGE_KOJI
  interruptible: true
  before_script:
    - if [[ -z "$KOJICI_USER" ]]; then echo "Variable KOJICI_USER not defined"; exit 1; fi
    - if [[ -z "$KOJICI_PWD" ]]; then echo "Variable KOJICI_PWD not defined"; exit 1; fi
    - mkdir koji
    - echo "${KOJICI_PWD}" | kinit ${KOJICI_USER}@CERN.CH
    - export BUILDNAME=$(rpm -qp --qf "%{n}-%{v}-%{r}\n" build/SRPMS/*${DIST}.src.rpm)
    - echo "Build name is \"${BUILDNAME}\""
    - export _KOJITAG_OS="KOJI_TAG_${_KOJI_OS}"
    - export _KOJITAG=${!_KOJITAG_OS:-"${KOJI_TAG}${_KOJI_OS}"}
  dependencies: []
  variables:
    GIT_STRATEGY: none

.koji_scratch:
  extends: .koji_deps
  stage: koji_scratch
  script:
    - pwd && echo $(pwd)
    - sleep 1000
    - echo "koji -p ${_KOJI_PROFILE} build --wait --scratch ${_KOJITAG} build/SRPMS/*${DIST}.src.rpm"
    - koji -p ${_KOJI_PROFILE} build --wait --scratch ${_KOJITAG} build/SRPMS/*${DIST}.src.rpm | tee taskid
    - export TASKID=$(sed -z -e 's/.*Created task:\ \([0-9]\+\).*/\1/g' taskid)
    - cd koji
    - koji -p ${_KOJI_PROFILE} download-task --noprogress $TASKID
    - export LOCAL_VERSION=$(rpm -qp --qf "%{v}-%{r}\n" ../build/SRPMS/*${DIST}.src.rpm)
    - export KOJI_VERSION=$(rpm -qp --qf "%{v}-%{r}\n" *.src.rpm)
    - echo "Local version is ${LOCAL_VERSION}, Koji version is ${KOJI_VERSION}"
    - |
      if [[ "$KOJI_VERSION" != "$LOCAL_VERSION" ]]; then
        echo "The version created locally does not match the one created by Koji."
        echo "Make sure you specify the DIST_${_KOJI_OS} variable correctly in your CI configuration."
        exit 1
      fi
  artifacts:
    expose_as: 'Koji scratch RPMs'
    paths:
      - koji/
    expire_in: 1 month

.koji_build:
  extends: .koji_deps
  stage: koji_build
  only:
    refs:
      - tags
  script:
    - BUILDEXISTS=$(koji list-builds --quiet --buildid="${BUILDNAME}" > /dev/null 2>&1; echo $?)
    - |
      if [ $BUILDEXISTS -eq 0 ]; then
        echo "Build ${BUILDNAME} already exists in Koji, please update the version number"
        exit 1
      fi
    - echo "koji -p ${_KOJI_PROFILE} build --wait ${_KOJITAG} build/SRPMS/*${DIST}.src.rpm"
    - koji -p ${_KOJI_PROFILE} build --wait ${_KOJITAG} build/SRPMS/*${DIST}.src.rpm
    - cd koji
    - koji -p ${_KOJI_PROFILE} download-build --noprogress --debuginfo ${BUILDNAME}
  artifacts:
    expose_as: 'Koji RPMs'
    paths:
      - koji/
    expire_in: 1 week

.tag_qa:
  extends: .koji_deps
  stage: deploy_qa
  when: manual
  only:
    refs:
      - tags
  script:
    - echo "koji -p ${_KOJI_PROFILE} tag-build ${_KOJITAG}-qa ${BUILDNAME}"
    - koji -p ${_KOJI_PROFILE} tag-build ${_KOJITAG}-qa ${BUILDNAME}
  allow_failure: false

.tag_stable:
  extends: .koji_deps
  stage: deploy_stable
  when: manual
  only:
    refs:
      - tags
  script:
    - echo "koji -p ${_KOJI_PROFILE} tag-build ${_KOJITAG}-stable ${BUILDNAME}"
    - koji -p ${_KOJI_PROFILE} tag-build ${_KOJITAG}-stable ${BUILDNAME}
